package com.sdm.dao;


import com.sdm.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class UserDAO {

    private static final Logger logger = LoggerFactory.getLogger(UserDAO.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Value("${sdmservice.user.fetch:query}")
    private String userFetchQuery;

    public User findByUserName(String username) {
        logger.info("UserDAO : loading user by username " + username);
        logger.info("UserDAO : query to load username is  " + userFetchQuery);
        User user = jdbcTemplate.queryForObject(
                userFetchQuery,new Object[]{username},
                new RowMapper<User>() {
                    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
                        User c = new User();
                        c.setUserId(rs.getInt("ID"));
                        c.setUserName(rs.getString("LOGIN_ID"));
                        c.setPassword(rs.getString("LOGIN_PASSWORD"));
                        c.setUserEmail(rs.getString("LOGIN_ID"));
                        return c;
                    }
                });
        return user;
    }
}
